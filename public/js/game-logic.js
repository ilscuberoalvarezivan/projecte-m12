var mech_counter = 0;
var selectedPlayer;
var teamRotation = 0;
var mode = 0;
var players = [];
var playerTeam = 1;
var map = document.getElementById("map");
var team0 = document.getElementById("team0");
var team1 = document.getElementById("team1");
var lastIntervalId;
var invertedView = false;
var ongoingAnimation = false;
var mouseHold = false;
var dragStartX;
var dragStartY;
var tiles = [];
const tileSize = 72;
const colNum = 50;
const rowNum = 20;

const orientations = [0, 180, -60, -120, 60, 120];

var row = document.getElementById("row1");





window.addEventListener('resize', function (event) {
    resizeElements();
}, true);

function resizeElements() {
    if (window.innerHeight > window.innerWidth) {
        map.style.width = (window.innerHeight) + "px";
        map.style.height = (window.innerWidth) + "px";

        if (playerTeam % 2 == 0) {
            map.style.transform = "rotate(" + -90 + "deg)";
        } else {
            map.style.transform = "rotate(" + 90 + "deg)";
        }

    } else {
        map.style.height = (window.innerHeight) + "px";
        map.style.width = (window.innerWidth) + "px";


        if (playerTeam % 2 != 0) {
            map.style.transform = "rotate(" + 180 + "deg)";
            invertedView = true;
        }
    }
}

function coordToPxl(x, y) {
    return [(tileSize * x + (tileSize / 2) * (y % 2)), (tileSize * y - (tileSize / 4) * y - y)];

}

function coordToId(x, y) {
    return x + y * colNum;
}

function getTileByCoord(x, y) {
    return tiles[coordToId(x, y)];
}

function dist(x1, x2, y1, y2) {
    return Math.sqrt((x2 -= x1) * x2 + (y2 -= y1) * y2);
}
function showPlayer(player) {
    showCoord(player.pos_x, player.pos_y);
}
function showCoord(posx, posy) {

    if (lastIntervalId != undefined) {
        window.clearInterval(lastIntervalId);
    }

    var [posx, posy] = coordToPxl(posx, posy);
    [posx, posy] = [posx - map.clientWidth / 2 + tileSize / 2, posy - map.clientHeight / 2 + tileSize / 2];
    [posx, posy] = [Math.floor(posx), Math.floor(posy)];
    var intervalID = window.setInterval(function () {
        var [stepX, stepY] = [Math.floor((posx - map.scrollLeft) / 10), Math.floor((posy - map.scrollTop) / 10)]
        map.scrollLeft += stepX;
        map.scrollTop += stepY;
        if ((map.scrollLeft in [posx, 0, map.sc] || stepX in [0, 1]) && (map.scrollTop in [posy, 0, map.clientHeight] || stepY in [0, 1])) {
            window.clearInterval(intervalID);
            lastIntervalId = undefined;
        }
    }, 10);
    lastIntervalId = intervalID;
    setTimeout(function () {
        if (lastIntervalId != undefined) {
            window.clearInterval(lastIntervalId);
        }
    }, 1100);
}



class Weapon {
    constructor(id) {
        switch (id) {
            case 0:
                this.name = "Energy Weapon";
                this.range = 10;
                this.damage = 4;
                this.nShots = 1;
                this.shotColor = "#05E7FF"
                break;
            case 1:
                this.name = "Canon";
                this.range = 7;
                this.damage = 6;
                this.nShots = 1;
                this.shotColor = "#FFF005"
                break;
            case 2:
                this.name = "Auto Canon";
                this.range = 7;
                this.damage = 4;
                this.nShots = 8;
                this.shotColor = "#FF9B05"
                break;
            case 3:
                this.name = "Missile Launch";
                this.range = 7;
                this.damage = 3;
                this.nShots = 3;
                this.shotColor = "#FF0505"
                break;

        }
    }
}

class Player {
    constructor(pilot_name = "Pilot " + mech_counter, mech_name = "Mech " + mech_counter, weapon1_id, weapon2_id, tileSize, posx = 0, posy = 0, rotation = 0, team = 0) {
        this.hp = 50;
        this.maxhp = 50;
        this.id = mech_counter++;
        this.pos_x = posx;
        this.pos_y = posy;
        this.team = team;
        this.tileSize = tileSize;
        this.mov = 5;
        this.pilot_name = pilot_name;
        this.mech_name = mech_name;
        this.weapon1 = new Weapon(weapon1_id);
        this.weapon2 = new Weapon(weapon2_id);
        this.moving = false;
        this.rotation = rotation;
        this.movPoints = 5;

        this.createGraphics();
        this.createHud();

        this.tile = tiles[this.pos_x + this.pos_y * colNum];
        if (players[team] == null) {
            players[team] = [this];
        } else {
            players[team][players[team].length] = this;
        }

    }

    createGraphics() {
        this.div = document.createElement("div");
        this.div.classList.add("player");
        this.div.style.width = this.tileSize + "px";
        this.div.style.height = this.tileSize + "px";
        this.div.style.left = (this.tileSize * this.pos_x + (this.tileSize / 2) * (this.pos_y % 2)) + "px";
        this.div.style.top = (this.tileSize * this.pos_y - (this.tileSize / 4) * this.pos_y - this.pos_y) + "px";
        this.div.style.backgroundImage = "url(/imgs/mechs/Mech-1.png)";
        const esto = this;
        this.div.addEventListener("click", function () {
            if (esto.team == selectedPlayer.team && esto.movPoints > 0) {
                mode = 0;
                selectedPlayer = esto
                resetTiles();
            }

            if (esto != selectedPlayer && esto.team != selectedPlayer.team && mode == 2 && esto.tile.isSelected) {
                selectedPlayer.shotMech(esto);
            }
        });
        this.div.style.transform = "rotate(" + this.rotation + "deg)";
        map.appendChild(this.div);
    }

    createHud() {
        var hud = document.createElement("div");
        hud.classList.add("hud" + this.team);
        var border = this.team == 0 ? "border-dark" : "border-white";
        var table = this.team == 0 ? "table" : "table table-dark";
        hud.innerHTML =
            '<h4>' + this.pilot_name + '</h4>' +
            '<h6>' + this.mech_name + '</h6>' +
            '<div class="progress border ' + border + '">' +
            '<div class="progress-bar border  ' + border + '  bg-success" id="hp' + this.id + '" role="progressbar" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '<div class="progress border  ' + border + '" style="margin-top: 5px;">' +
            '<div class="progress-bar border  ' + border + ' bg-warning" id="action' + this.id + '-0" role="progressbar" style="width: 50%">' +
            '</div>' +
            '<div class="progress-bar border  ' + border + ' bg-warning" id="action' + this.id + '-1" role="progressbar" style="width: 50%">' +
            '</div>' +
            '</div>' +
            '<table class="' + table + '">' +
            // '<thead>' +
            // '<tr>' +
            // '<th scope="col">Name</th>' +
            // '<th scope="col">Damage</th>' +
            // '<th scope="col">Range</th>' +
            // '<th scope="col">nShoots</th>' +
            // '</tr>' +
            // '</thead>' +
            // '<tbody>' +
            // '<tr>' +
            // '<th scope="row" style="color: ' + this.weapon1.shotColor + '>' + this.weapon1.name + '</th>' +
            // '<td>' + this.weapon1.damage + '</td>' +
            // '<td>' + this.weapon1.range + '</td>' +
            // '<td>' + this.weapon1.nShots + '</td>' +
            // '</tr>' +
            // '<tr>' +
            // '<th scope="row" style="color: ' + this.weapon2.shotColor + ';">' + this.weapon2.name + '</th>' +
            // '<td>' + this.weapon2.damage + '</td>' +
            // '<td>' + this.weapon2.range + '</td>' +
            // '<td>' + this.weapon2.nShots + '</td>' +
            // '</tr>' +
            // '</tbody>' +
            '</table>';

        this.hud = hud;

        if (this.team == 0) {
            team0.appendChild(this.hud);
        } else {
            team1.appendChild(this.hud);
        }

        this.hpBar = document.getElementById("hp" + this.id);
        this.actionBar = [];
        this.actionBar[0] = document.getElementById("action" + this.id + "-0");
        this.actionBar[1] = document.getElementById("action" + this.id + "-1");

    }

    shotMech(player) {
        var [objectiveX, objectiveY] = coordToPxl(player.pos_x, player.pos_y);
        var [playerX, playerY] = coordToPxl(this.pos_x, this.pos_y);

        var delta_x = objectiveX - playerX;
        var delta_y = objectiveY - playerY;
        var rotation = Math.atan2(delta_y, delta_x) * (180 / Math.PI);
        console.log(rotation);
        const esto = this;
        resetTiles()
        this.div.animate([{}, { "transform": "rotate(" + rotation + "deg)" }], { "duration": (this.rotation != rotation) * 500, "fill": "forwards", "easing": "ease-in-out" }).onfinish = function () {
            var distancia = dist(playerX, objectiveX, playerY, objectiveY)
            if (esto.weapon1.range * tileSize >= distancia) {
                var bullet = document.createElement("img");
                bullet.setAttribute("src", "/imgs/mechs/Mech-1.png");
                bullet.style.position = "absolute";
                bullet.style.width = esto.tileSize + "px";
                bullet.style.height = esto.tileSize + "px";
                bullet.style.left = playerX + "px";
                bullet.style.top = playerY + "px";
                map.appendChild(bullet);
                bullet.animate([{}, { "transform": "rotate(" + rotation + "deg)" }], { "duration": 0, "fill": "forwards" }).onfinish = function () {
                    bullet.animate([{}, { "left": objectiveX + "px", "top": objectiveY + "px" }], { "duration": distancia, "fill": "forwards", "easing": "ease-in" }).onfinish = function () {
                        player.damage(esto.weapon1.damage);
                        map.removeChild(bullet);
                    }
                }

            }
            /*
            if (esto.weapon2.range >= distancia) {
                player.damage(esto.weapon2.damage);
            }
            */
            esto.setActions(0);
            nextMember();
        };

    }

    damage(damage) {
        this.hp -= damage;
        this.hpBar.animate([{}, { "width": Math.floor((this.hp / this.maxhp) * 100) + "%" }], { "duration": damage * 100, "fill": "forwards" });
        if (this.hp < 0) {
            const esto = this;
            this.div.animate([{}, { "width": "0%", "height": "0%" }], { "duration": 2000, "fill": "forwards" }).onfinish = function () {
                map.removeChild(esto.div);
                var array = players[esto.team];
                const index = array.indexOf(esto);
                if (index > -1) {
                    array.splice(index, 1);
                }
                if (esto.team == 0) {
                    team0.removeChild(esto.hud);
                } else {
                    team1.removeChild(esto.hud);
                }
            };
        }

    }

    setActions(val) {
        this.movPoints = val;
        // switch (this.movPoints) {
        //     case 0:
        //         this.actionBar[0].animate([{}, { "width": "0%" }], { "duration": 1000, "fill": "forwards" });
        //         this.actionBar[1].animate([{}, { "width": "0%" }], { "duration": 1000, "fill": "forwards" });
        //         break
        //     case 1:
        //         this.actionBar[0].animate([{}, { "width": "50%" }], { "duration": 1000, "fill": "forwards" });
        //         this.actionBar[1].animate([{}, { "width": "0%" }], { "duration": 1000, "fill": "forwards" });
        //         break
        //     case 2:
        //         this.actionBar[0].animate([{}, { "width": "50%" }], { "duration": 1000, "fill": "forwards" });
        //         this.actionBar[1].animate([{}, { "width": "50%" }], { "duration": 1000, "fill": "forwards" });
        //         break
        // }
    }

    calcMovRotation(originTile, targetTile) {
        return orientations[originTile.getNearTiles().indexOf(targetTile)];
    }

    moveTo(targetx, targety) {

        var nextTile = this.tile.road.shift();
        var posx, posy;
        if (nextTile == undefined) {
            [posx, posy] = [targetx, targety];
        } else {
            [posx, posy] = [nextTile.posx, nextTile.posy];
        }
        var [pxlX, pxlY] = coordToPxl(posx, posy);
        var end = { "top": pxlY + "px", "left": pxlX + "px" };
        var mov = [{}, end];
        var options = { "duration": 250, "fill": "forwards" };
        var [currentTile, targetTile] = [getTileByCoord(this.pos_x, this.pos_y), getTileByCoord(posx, posy)];
        var rotation = this.calcMovRotation(currentTile, targetTile);
        this.rotation = rotation;
        if (this.pos_x - targetx != 0 || this.pos_y - targety != 0) {

            this.moving = true;
            resetTiles();
            const esto = this;

            this.div.animate([{}, { "transform": "rotate(" + this.rotation + "deg)" }], { "duration": 0, "fill": "forwards" }).onfinish = function () {
                esto.div.animate(mov, options).onfinish = function () {

                    esto.pos_x = posx;
                    esto.pos_y = posy;

                    if (esto.pos_x - targetx != 0 || esto.pos_y - targety != 0) {
                        esto.moveTo(targetx, targety);
                    } else if (esto.movPoints > 0) {
                        esto.moving = false;
                        movementMode();
                        showPlayer(esto);
                    } else {
                        esto.moving = false;
                        resetRoads();
                        nextMember();
                    }
                };
            };

        }
    }



};


class Tile {
    constructor(id, size, mapWidth) {
        this.id = id;
        this.posy = Math.floor(id / mapWidth);
        this.posx = id % mapWidth;
        this.nearTilesIds = [
            id + 1, //derecha
            id - 1, //izquierda
            id - mapWidth + this.posy % 2, //arriba derecha
            id - mapWidth + this.posy % 2 - 1, //arriba izquierda
            id + mapWidth + this.posy % 2, //abajo derecha
            id + mapWidth + this.posy % 2 - 1 //abajo izquierda
        ];

        this.currentOpacity = "0.0";

        this.baseColor = "darkgreen";
        this.bgcolor = "darkgreen";
        this.isSelected = false;
        this.road = [];
        this.movCost = 0;

        this.createDivs(size);
        this.createEventListeners();
    }

    createDivs(size) {
        this.div = document.createElement("div");
        this.div.classList.add("tile");
        this.div.style.width = size + "px";
        this.div.style.height = size + "px";
        this.div.style.left = (size * this.posx + (size / 2) * (this.posy % 2)) + "px";
        this.div.style.top = (size * this.posy - (size / 4) * this.posy - this.posy) + "px";
        this.div.style.backgroundImage = "url(/imgs/tiles/Grass" + (this.id % 5 + 1) + ".png)";
        this.div.style.backgroundSize = size + "px";

        this.hoverImg = document.createElement("div");
        this.hoverImg.classList.add("tile");
        this.hoverImg.style.width = size + "px";
        this.hoverImg.style.height = size + "px";
        this.hoverImg.style.left = (size * this.posx + (size / 2) * (this.posy % 2)) + "px";
        this.hoverImg.style.top = (size * this.posy - (size / 4) * this.posy - this.posy) + "px";
        this.hoverImg.style.backgroundImage = "url(/imgs/tiles/Select-Hexagon.png)";
        this.hoverImg.style.backgroundSize = size + "px";
        this.hoverImg.style.opacity = "0";
    }

    createEventListeners() {
        const esto = this;
        this.hoverImg.addEventListener("mouseenter", function (e) {
            if (!selectedPlayer.moving) {
                esto.hoverImg.style.opacity = "0.8";
                esto.road.forEach(function (val) {
                    val.hoverImg.style.opacity = "0.8";
                });
            }
        });
        this.hoverImg.addEventListener("mouseleave", function (e) {
            e.target.style.opacity = esto.currentOpacity;
            esto.road.forEach(function (val) {
                val.hoverImg.style.opacity = val.currentOpacity;
            });
        });
        this.hoverImg.addEventListener("mousedown", function (e) {

            if (mode == 1 && esto.isSelected && selectedPlayer != null && !selectedPlayer.moving) {
                selectedPlayer.setActions(selectedPlayer.movPoints - esto.movCost);
                resetTiles();
                selectedPlayer.tile = esto;
                selectedPlayer.moveTo(esto.posx, esto.posy);
            } else {
                mouseHold = true;
                dragStartX = e.clientX;
                dragStartY = e.clientY;
            }
        });
        this.hoverImg.addEventListener("mousemove", function (e) {
            if (mouseHold) {
                map.scrollTop += (dragStartY - e.clientY) * (1 - 2 * (invertedView));
                map.scrollLeft += (dragStartX - e.clientX) * (1 - 2 * (invertedView));
                dragStartX = e.clientX;
                dragStartY = e.clientY;
            }
        });
        this.hoverImg.addEventListener("mouseup", function (e) {
            mouseHold = false;
        });
    }

    changeBgImg(img) {
        //this.bgcolor = img;
        //this.div.style.backgroundColor = img;
        this.hoverImg.style.backgroundImage = "url(/imgs/tiles/" + img + ")";
    }

    setMovCost(cost) {
        this.movCost = cost;
    }

    resetTile() {
        //this.bgcolor = this.baseColor;
        //this.div.style.backgroundColor = tile.bgcolor;
        this.isSelected = false;
        this.movCost = 0;
        this.hoverImg.style.opacity = "0";
        this.currentOpacity = "0";
        this.hoverImg.style.backgroundImage = "url(/imgs/tiles/Select-Hexagon.png)";
    }

    getNearTiles() {
        var nearTiles = [];
        var tile;
        var [thisX, thisY] = [this.posx, this.posy];
        this.nearTilesIds.forEach(function (val) {
            if (val > 0 && val < tiles.length) {
                tile = tiles[val];
                if (tile.posy == thisY && (thisX == tile.posx + 1 || thisX == tile.posx - 1) || ((thisY == tile.posy + 1 || thisY == tile.posy - 1) && (thisX == tile.posx || thisX == tile.posx - 1 + 2 * (thisY % 2 == 0)))) {
                    nearTiles.push(tile);
                }
            }
        });
        return nearTiles;
    }
};



for (var i = 0; i < colNum * rowNum; i++) {
    var tile = new Tile(i, tileSize, colNum);
    tiles[tiles.length] = tile;
    map.appendChild(tile.div);
    map.appendChild(tile.hoverImg);

}

function resetTiles() {
    tiles.forEach(function (tile) {
        tile.resetTile();
    })
    if (!selectedPlayer.moving) {
        selectedPlayer.tile.changeBgImg("Select-Hexagon.png");
    }

}
function resetRoads() {
    tiles.forEach(function (tile) {
        tile.road = [];
    })
}

function playerInTile(tile) {
    for (let i = 0; i < players.length; i++) {
        for (let j = 0; j < players[i].length; j++) {
            if (players[i][j].tile == tile) {
                return true;
            }
        }
    }
    return false;
}

function paintCurrentTeamTiles() {
    players[selectedPlayer.team].forEach(function (player) {
        let playerTile = player.tile;
        let opacity = "0.5";
        if (player == selectedPlayer) {
            playerTile.isSelected = true;
            opacity = "0.8";
        }
        playerTile.hoverImg.style.opacity = opacity;
        playerTile.changeBgImg("Selected-Hexagon.png");
        playerTile.currentOpacity = "0.8";
    })
}

function movementMode() {
    resetRoads();
    resetTiles();
    mode = 1;
    var range = selectedPlayer.movPoints;
    var playerTile = selectedPlayer.tile;
    paintCurrentTeamTiles()
    var moveTiles = playerTile.getNearTiles();
    let currentTile;
    while (moveTiles.length > 0) {
        currentTile = moveTiles.shift();
        if (playerInTile(currentTile)) {
            continue;
        }
        currentTile.isSelected = true;
        currentTile.hoverImg.style.opacity = "0.5";
        currentTile.changeBgImg("Movement-Hexagon.png");
        currentTile.currentOpacity = "0.5";
        currentTile.movCost = currentTile.movCost + 1;
        if (currentTile.movCost < range) {
            currentTile.getNearTiles().forEach(function (tile) {
                if (!tile.isSelected && !moveTiles.includes(tile)) {
                    tile.movCost = currentTile.movCost;
                    tile.road = [...currentTile.road];
                    tile.road.push(currentTile);
                    moveTiles.push(tile);
                }
            });
        }
    }
}

function shootMode() {
    resetRoads();
    resetTiles();
    mode = 2;
    var [playerPosX, playerPosY] = coordToPxl(selectedPlayer.pos_x, selectedPlayer.pos_y);
    tiles.forEach(function (tile) {
        let [tilePosX, tilePosY] = coordToPxl(tile.posx, tile.posy);
        var distance = dist(playerPosX, tilePosX, playerPosY, tilePosY);
        if (distance <= selectedPlayer.weapon1.range * tileSize) {
            //console.log(distance + "-" + selectedPlayer.weapon1.range * tileSize);
            tile.isSelected = true;
            tile.hoverImg.style.opacity = "0.5";
            tile.currentOpacity = "0.5";
            tile.changeBgImg("Attack-Hexagon.png");
        }
    });
    for (var i = 0; i < players.length; i++) {
        if (i == selectedPlayer.team) {
            continue;
        }

        var enemies = players[i];
        enemies.forEach(function (enemy) {
            let [enemyPosX, enemyPosY] = coordToPxl(enemy.pos_x, enemy.pos_y);
            var distance = dist(playerPosX, enemyPosX, playerPosY, enemyPosY);
            if (distance <= selectedPlayer.weapon1.range * tileSize) {
                enemy.tile.hoverImg.style.opacity = "0.8";
                enemy.tile.currentOpacity = "0.8";
                enemy.tile.changeBgImg("Attack-Hexagon.png");
            }
        })
    }
    paintCurrentTeamTiles()
}

function nextMember() {
    var team = players[teamRotation];
    var nextPlayer;
    for (let i = 0; i < team.length; i++) {
        if (team[i].movPoints > 0 && team[i].hp > 0) {
            nextPlayer = team[i];
        }
    }
    if (nextPlayer != null) {
        selectedPlayer = nextPlayer;
        resetTiles();
        movementMode();
        showPlayer(selectedPlayer);
    } else {
        switchTeam();
    }
}

function switchTeam() {
    teamRotation++;
    teamRotation = teamRotation % players.length;
    players[teamRotation].forEach(function (player) {
        player.setActions(player.mov);
    })
    nextMember();
}



player = new Player("Kevencio", "aaaaaaaa", 1, 1, tileSize, 15, 10, 50, 0);
player = new Player("Rogelio", "aaaaaaaa", 1, 1, tileSize, 15, 13, 50, 0);
player = new Player("aaa", "aaaaaaaa", 1, 1, tileSize, 20, 10, 50, 1);
player = new Player("aaa", "aaaaaaaa", 1, 1, tileSize, 20, 13, 50, 1);

resizeElements();

nextMember();

movementMode();









