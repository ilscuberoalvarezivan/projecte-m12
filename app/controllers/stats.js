var moongose = require("mongoose"),
    docente = require("../models/docente");

exports.load = async () => {
    var res = await docente.find({})
    return res;
}
exports.save = (req) => {
    var newAsignatura = new docente(req);
    newAsignatura.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}