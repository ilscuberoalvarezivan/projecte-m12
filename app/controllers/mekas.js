var moongose = require("mongoose"),
    alumno = require("../models/alumno");

exports.load = async () => {
    var res = await alumno.find({})
    return res;
}
exports.save = (req) => {
    var newAlumno = new alumno(req);
    newAlumno.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}