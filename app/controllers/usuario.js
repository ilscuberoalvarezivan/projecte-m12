var moongose = require("mongoose"),
    asignatura = require("../models/asignatura");

exports.load = async () => {
    var res = await asignatura.find({})
    return res;
}
exports.save = (req) => {
    var newAsignatura = new asignatura(req);
    newAsignatura.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}