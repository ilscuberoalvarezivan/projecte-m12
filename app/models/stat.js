var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var asignaturaSchema = new Schema({
    nombre: { type: String },
    numHoras: { type: String },
    docente: { type: mongoose.Types.ObjectId },
    alumnos: { type: [mongoose.Types.ObjectId] },
});

module.exports = mongoose.model("asignatura", asignaturaSchema);