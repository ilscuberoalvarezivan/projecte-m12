var express = require("express");
router = express.Router();

router.route("/partida/:sala").get(async (req, res, next) => {
    res.render("partida", { "sala": req.params.sala });
});


module.exports = router;
